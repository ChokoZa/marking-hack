# Команда S1mpl3_sm1l3

# Решение задачи №2 "Система предиктивной аналитики рынка на основе данных «Честного знака»"

## Введение

> Современный мир насыщен огромным объемом цифровой информации, который постоянно увеличивается. Традиционные аналитические инструменты больше не справляются с обработкой и анализом такого объема данных. Для повышения эффективности работы компаний и служб необходимо использование новых, более продвинутых инструментов. Создание системы предиктивной аналитики рынка позволит эффективно обрабатывать большие объемы данных и предсказывать тенденции на рынке, что в свою очередь повысит конкурентоспособность компаний. Кроме того, потребители смогут получить прозрачную информацию о производстве, сбыте и перемещении товаров, что способствует улучшению отношений между компаниями и клиентами.

## Актуальность проекта

> В настоящее время создание инструментов для ускорения и улучшения работы государственных служб является необходимостью. Быстрое и точное принятие решений, доступность информации для граждан, а также повышение экономических показателей предприятий - это главные задачи, стоящие перед государственными структурами. Поэтому создание инструментов предиктивной аналитики и машинного обучения позволит существенно ускорить работу государственных служб и повысить качество принимаемых решений. Более того, такие инструменты помогут улучшить механизм изучения обстановки и прогнозирования тенденций в различных сферах, что, в свою очередь, повысит эффективность работы государственных структур и обеспечит высокий уровень обслуживания для граждан.

## Цели проекта

1. Разработка программного комплекса для визуализации большого объема данных - это позволит государственным службам эффективно обрабатывать и анализировать большие объемы информации, что позволит принимать более точные и обоснованные решения.
2. Повышение качества работы информационно-аналитических служб - создание новых инструментов для обработки и анализа данных поможет ускорить работу служб и улучшить качество принимаемых решений.
3. Разработка системы предиктивной аналитики для определения государственной политики - это поможет государственным структурам более точно определять тенденции и прогнозировать развитие в различных сферах, что позволит принимать более эффективные решения в интересах граждан и страны в целом.
4. Повышение уровня обеспечения информацией граждан - благодаря созданию новых инструментов для анализа и обработки данных, граждане получат доступ к более точной и полной информации, что позволит им более осознанно принимать решения в различных сферах жизни, таких как здравоохранение, образование и экономика.

## Задачи проекта

1. Анализ наборов данных - это важная задача, которая позволяет определить основные параметры и характеристики данных, выявить закономерности и тренды, которые могут быть использованы для принятия решений.
2. Создание базы данных - это задача, которая позволит хранить и управлять большим объемом данных, необходимых для работы системы. База данных должна быть создана таким образом, чтобы обеспечить быстрый доступ к нужным данным и обеспечить безопасность информации.
3. Создание механизма визуализации ввода и вывода товара в оборот - эти задачи связаны с отслеживанием перемещения товара через различные этапы его жизненного цикла, что позволит оптимизировать его продажу и снизить затраты на логистику.
4. Создание маршрутной карты товара - это задача, которая позволит отслеживать перемещение товара в режиме реального времени, что позволит предсказывать возможные задержки и проблемы в его перемещении.
5. Разработка системы предиктивной аналитики - это задача, которая позволит прогнозировать развитие рынка и принимать обоснованные решения на основе анализа данных.
6. Создание Desktop-приложения - это задача, которая позволит пользователям получать доступ к системе на своих компьютерах и работать с данными в удобном интерфейсе.

## Основные этапы решения задачи

1. Сбор и анализ данных: необходимо собрать большое количество данных о рынке и проанализировать их, чтобы выявить закономерности, тренды и особенности развития рынка.
2. Выбор и разработка методов и алгоритмов анализа: на основе полученных данных нужно выбрать наиболее подходящие методы и алгоритмы анализа, которые позволят получить максимально точные прогнозы.
3. Разработка системы предиктивной аналитики: на основе разработанных моделей и алгоритмов нужно создать систему предиктивной аналитики, которая будет предсказывать изменения на рынке и помогать принимать обоснованные решения.
4. Тестирование и настройка системы: после создания системы необходимо ее протестировать и настроить, чтобы она работала максимально эффективно и точно предсказывала изменения на рынке.
5. Реализация и внедрение системы: после тестирования и настройки системы она готова к внедрению и использованию в реальных условиях.

## Перспективы развития проекта:

1. Расширение набора данных: По мере роста доступности данных, можно расширять наборы, используемые для анализа, что позволит получать более точные прогнозы и предсказания.
2. Использование новых методов анализа: С развитием искусственного интеллекта и машинного обучения появляются новые методы анализа, которые могут применяться для улучшения алгоритмов системы предиктивной аналитики рынка.
3. Оптимизация программного комплекса: Оптимизация работы программного комплекса позволит ускорить обработку данных и улучшить производительность системы, что повысит ее эффективность.
4. Внедрение новых функциональных возможностей: Добавление новых функциональных возможностей, таких как машинное обучение, искусственный интеллект и глубокое обучение в аналитике и визуализации данных, позволит расширить функциональность системы и повысить качество анализа данных.

## Особенности решения

* Во-первых, мы используем продвинутые инструменты визуализации данных, что позволяет наглядно представлять информацию о рынке и его тенденциях. Благодаря этому аналитики и менеджеры могут быстро и точно оценивать ситуацию на рынке, а также определять проблемные зоны и возможности для развития.
* Во-вторых, наше решение включает создание интерактивных карт ввода и вывода товара из оборота. Это позволяет легко отслеживать перемещение товаров, следить за их оборотом, а также управлять процессом их сбыта и производства.
* В-третьих, мы создаем маршрутную карту перемещения товара, что позволяет более точно прогнозировать его движение на рынке. Это обеспечивает возможность оптимизации логистических процессов и управления производством в целом.
* Таким образом, наше решение не только обеспечивает предиктивную аналитику рынка, но и позволяет использовать передовые инструменты визуализации данных, что дает значительные преимущества в принятии решений и управлении процессами.

## Desktop-приложение

Преимущества разработки Desktop приложения:

1. Локальное хранение данных: Десктопное приложение обеспечивает локальное хранение данных, что гарантирует безопасность и конфиденциальность информации, а также обеспечивает доступ к данным даже при отсутствии интернет-соединения.
2. Быстродействие: Приложение, работающее локально на компьютере, обеспечивает быстрый доступ к данным и высокую скорость обработки информации.
3. Удобство использования: Десктопное приложение обычно имеет более интуитивно понятный и удобный интерфейс, что делает его более простым и удобным в использовании, чем веб-приложение.
4. Большая гибкость: Разработка десктопного приложения дает большую гибкость в выборе языка программирования, технологий и архитектур, что позволяет создавать более мощные и эффективные приложения.
5. Лучшее управление ресурсами: Десктопное приложение может лучше управлять ресурсами компьютера, такими как процессор, оперативная память и графический процессор, что позволяет обеспечить более быструю и эффективную работу приложения.

## Используемые технологии

Мы используем передовые технологии для создания нашей системы предиктивной аналитики рынка, включая библиотеку folium для визуализации данных на карте, pyQt5 для создания красивого и функционального интерфейса, язык программирования Python для удобства и эффективности написания кода, формат JSON для обмена данными с другими системами, библиотеку pandas для работы с данными, randomForestRegression и scikit learn для создания точных моделей предиктивной аналитики и sqLite для создания базы данных для хранения и управления информацией. Использование этих технологий обеспечивает высокую точность прогнозирования и повышает качество работы нашей системы.

## Дополнительные возможности

Наша программа для предиктивной аналитики рынка обладает дополнительными возможностями, которые могут значительно упростить и расширить процесс анализа данных и принятия решений:

1. Измерение расстояний и площадей на карте позволяет оценить географические характеристики рынка и его структуру.
2. Возможность добавления новых меток и определения координат позволяет улучшить точность анализа данных и их визуализации.
3. Управление слоями позволяет работать с различными категориями данных и управлять их отображением.
4. Нанесение геометрических фигур позволяет выделить на карте определенные области и провести более детальный анализ данных.
5. Поиск по адресу или названию позволяет быстро находить нужные точки на карте и проводить анализ данных по определенным регионам.
6. Построение карт маршрутов товарных групп позволяет оптимизировать логистические процессы и снизить затраты на доставку.
7. Визуализация ввода и вывода товара из оборота в заданном регионе позволяет быстро оценить объемы продаж и скорректировать стратегию сбыта товаров.
8. Просмотр показателей продаж по каждой товарной группе позволяет анализировать эффективность работы компании и принимать оперативные решения.

## Ссылка на видеодемонстрацию и презентацию

Скринкаст находится в папке Диска:
https://drive.google.com/drive/folders/1Lccww9UV-wtgvEYZBvJrNVNaZ1QYIAz8?usp=sharing

Презентация:

https://docs.google.com/presentation/d/1hYotKrSAz9GF_nMdaxpG8BT3ZaWutjfl/edit?usp=sharing&ouid=105081270251138513165&rtpof=true&sd=true

## Заключение

Введение в эксплуатацию разработанного программного комплекса обеспечит повышение эффективности деятельности информационно-аналитических служб, предоставит возможность отслеживания изменений о ТТХ БПЛА и их комплектующих, сократит время на принятие правильных решений.

В заключение можно сказать, что введение в эксплуатацию разработанного программного комплекса позволит государственным службам значительно повысить эффективность своей деятельности. Благодаря возможности отслеживания перемещения товаров и визуализации ввода и вывода товара в оборот в заданном регионе, а также построению карт маршрутов товарных групп, информационно-аналитические службы смогут более точно определять тренды на рынке и принимать решения на основе точных данных.

Также, использование продвинутых инструментов визуализации, таких как folium, и возможности измерения расстояний, площадей на карте, добавления новых меток, определения координат и управления слоями, нанесения геометрических фигур, поиска по адресу или названию, позволят более удобно и точно работать с данными на карте.

Кроме того, система предиктивной аналитики, основанная на алгоритмах randomForestRegression и scikit learn, позволит более точно прогнозировать изменения на рынке и принимать правильные решения на основе точных данных.

Введение в эксплуатацию разработанного программного комплекса позволит существенно повысить экономические показатели предприятий и улучшить уровень обеспечения информацией гражда
