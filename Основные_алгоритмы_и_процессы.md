# Основные алгоритмы и процессы

## Интерактивная карта

> Класс Map используется в библиотеке Leaflet для отображения интерактивных карт на веб-страницах. Он содержит множество методов и свойств, которые позволяют добавлять различные элементы управления и слои на карту.
>
> Некоторые из дополнительных функций, которые можно добавить на карту, включают:
>
> 1. Geocoder - функция, которая позволяет пользователю выполнять геокодирование, то есть преобразовывать адреса в координаты и наоборот. Для реализации этой функции в Leaflet.js используется библиотека Leaflet Control Geocoder. Она подключается к карте с помощью метода `L.Control.geocoder()`. После этого пользователь может вводить адрес в специальном поле, а затем приложение отображает его на карте.
> 2. MousePosition - функция, которая позволяет пользователю получать информацию о координатах, на которых находится указатель мыши. Для реализации этой функции в Leaflet.js используется библиотека Leaflet Mouse Position. Она подключается к карте с помощью метода `L.Control.mousePosition()`. После этого пользователь может перемещать курсор мыши по карте, и на экране отображаются его текущие координаты.
> 3. MiniMap - функция, которая позволяет пользователю просматривать карту в миниатюрном размере. Для реализации этой функции в Leaflet.js используется библиотека Leaflet MiniMap. Она подключается к карте с помощью метода `L.Control.MiniMap()`. После этого пользователь может видеть карту в маленьком окне на карте и перемещать его, чтобы перемещаться по карте.
> 4. Fullscreen - функция, которая позволяет пользователю развернуть карту на весь экран. Для реализации этой функции в Leaflet.js используется библиотека Leaflet Fullscreen. Она подключается к карте с помощью метода `L.Control.Fullscreen()`. После этого пользователь может развернуть карту на весь экран и сжать его.
> 5. MeasureControl - функция, которая позволяет пользователю измерять расстояния на карте. Для реализации этой функции в Leaflet.js используется библиотека Leaflet MeasureControl. Она подключается к карте с помощью метода `L.Control.measureControl()`. После этого пользователь может щелкнуть на карте, чтобы установить точку начала измерения, а затем щелкнуть на карте, чтобы установить точку окончания измерения. Приложение покажет расстояние между точками в единицах измерения, выбранных пользователем.
> 6. Draw:
>    Draw реализует возможность рисования на карте при помощи мыши. При активации этой функции пользователь может выбрать тип фигуры (например, линия, круг, многоугольник) и начать рисовать на карте. Каждый раз, когда пользователь добавляет новую точку, фигура обновляется и рисуется на карте. Функция также позволяет пользователю сохранять нарисованные фигуры и изменять их позже.
> 7. TileLayer:
>    TileLayer является классом, используемым для загрузки и отображения тайловых карт (карт, разбитых на квадраты и загружаемых по мере необходимости). Он работает с помощью установки URL-адреса тайлового сервера, который возвращает тайлы с соответствующими координатами, и указания параметров, таких как максимальный и минимальный уровни масштабирования.
> 8. LayerControl:
>    LayerControl - это класс, позволяющий управлять отображаемыми на карте слоями. Он позволяет добавлять и удалять слои, а также переключать их видимость на карте. LayerControl может быть использован для управления любым числом слоев, включая TileLayer, Marker, GeoJSON и другие.
>
>    Каждая из перечисленных функций имеет свой набор методов и параметров, которые позволяют настраивать ее работу в зависимости от требований проекта. Обычно функции Geocoder, MousePosition, MiniMap, Fullscreen, MeasureControl, Draw, TileLayer, LayerControl используются вместе для создания полнофункциональной интерактивной карты веб-приложения.
> 9. Для создания объектов маркеров используется класс L.Marker из библиотеки Leaflet. При создании маркера указываются его координаты, и дополнительные параметры, такие как иконка, всплывающее окно с информацией и т.д.
>
>    Для создания кластеров маркеров используется плагин Leaflet.markercluster. Плагин создает кластеры из маркеров, которые находятся близко друг к другу на карте, и отображает количество маркеров в кластере.
>
>    Для создания групп маркеров используется класс L.LayerGroup из библиотеки Leaflet. Группа маркеров может содержать в себе несколько маркеров, кластеров маркеров и других групп маркеров. Группа маркеров может использоваться, например, для отображения маркеров разных категорий на карте в зависимости от выбора пользователя.
> 10. 

## Машинное обучение

Мы используем случайный лес для прогнозирования цен продукта на будущее.

Программа извлекает данные из базы данных SQLite, используя SQL-запрос для выборки всех записей, которые соответствуют заданному коду продукта, имеют цену, отличную от нуля. Затем, полученные данные преобразуются в объект Pandas DataFrame.

Для обучения модели случайного леса, данные разделяются на тренировочный и тестовый наборы. Затем, происходит выбор и обучение модели случайного леса с использованием тренировочных данных.

Затем, используя модель, происходит прогнозирование цен продукта на следующие 90 дней. Прогнозирование происходит для будущих дат с нулевым количеством продаж.

Наконец, график строится с помощью библиотеки Matplotlib, который отображает текущие продажи и предсказанные продажи продукта на временной оси в зависимости от цены продажи.

Данный метод используется для прогнозирования цены продукта в будущем на основе данных о его продажах в прошлом. В данной программе применяется метод случайного леса, который является одним из методов машинного обучения, позволяющим построить прогностическую модель.

Основные шаги метода:

1. Загрузка данных из базы данных SQLite. Данные содержат информацию о продажах продукта (указанный код продукта, дата продажи, цена продажи, количество проданного продукта).
2. Преобразование данных. Дата продажи преобразуется в объект типа datetime, цена продажи и количество проданного продукта преобразуются в соответствующие числовые значения. Создаются дополнительные признаки (weekday, month, year), которые отображают день недели, месяц и год продажи.
3. Выбор заданного продукта. Данные фильтруются по коду продукта, переданному в качестве параметра.
4. Разделение данных на тренировочный и тестовый наборы. Тренировочный набор используется для обучения модели, а тестовый набор - для оценки ее точности.
5. Обучение модели случайного леса. Модель создается с использованием библиотеки scikit-learn. В данной программе используются следующие параметры: n_estimators - количество деревьев в лесу, max_depth - максимальная глубина дерева, random_state - начальное значение для генератора случайных чисел.
6. Предсказание цены продукта на три месяца вперед. Создается набор дат, начиная с максимальной даты из исходных данных и продолжающийся на 90 дней вперед. Создается датафрейм с признаками для каждой даты (weekday, month, year, quantity), где quantity равно 0, так как количество продаж неизвестно. Далее для каждой даты в этом датафрейме предсказывается цена продукта с помощью модели случайного леса.
7. Визуализация результатов. Создается график, на котором отображаются текущие продажи и предсказанные продажи на заданный период времени.

Таким образом, метод случайного леса позволяет на основе исторических данных прогнозировать цену продукта на заданный период времени с достаточной точностью.
